<?php
/**
 * CORSharing - enabling CORS (Cross-Origin Resource Sharing)
 * Enable/disable CORS (Cross-Origin Resource Sharing). Used to enable CORS http headers to permits web client visiting other website to load some data from your dokuwiki website.
 * 
 * @author Cyrille Giquello <cyrille@comptoir.net>
 * @copyright 2016 Cyrille Giquello
 * @license LGPL v3 http://www.gnu.org/licenses/lgpl.html
 * @link http://savoirscommuns.comptoir.net
 */


use dokuwiki\Extension\ActionPlugin;

class action_plugin_corsharing extends DokuWiki_Action_Plugin {


	/**
	 * Register its handlers with the DokuWiki's event controller.
	 * 
	 * https://www.dokuwiki.org/devel:events
	 */
	function register( $controller) {
		$controller->register_hook('DOKUWIKI_INIT_DONE', 'AFTER', $this, 'handleEvent');
	}

	function handleEvent( $event, $param ) {
		header('Access-Control-Allow-Origin: *');
	}

}
